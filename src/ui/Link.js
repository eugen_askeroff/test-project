import { NavLink } from 'react-router-dom'
import emotion from '@emotion/styled/macro'

import { colors as theme } from './theme'

export const Link = emotion(NavLink)`
  color: ${theme.green};
  &:not(:hover) {
    text-decoration: none;
  }
`
