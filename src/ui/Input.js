import emotion from '@emotion/styled/macro'

import { colors as theme } from './theme'

export const Input = emotion.input`
  height: 38px;
  border: none;
  outline: none;
  border-bottom: 1px solid #455E70;
  font-size: 16px;
  color: ${theme.dark};
  width: 100%;
`
