import React from 'react'
import emotion from '@emotion/styled/macro'

import { colors } from './theme'

export class Alert extends React.PureComponent {
  render() {
    return (
      <Layer>
        <Backdrop onClick={this.props.onCancel}/>
        <Paper>
          <div>
            <Heading>
              Are you sure?
            </Heading>
            <Text>
              {this.props.text}
            </Text>
          </div>
          <Buttons>
            <Button onClick={this.props.onCancel}>CANCEL</Button>
            <Button onClick={this.props.onConfirm} color={colors.green}>OK</Button>
          </Buttons>
        </Paper>
      </Layer>
    )
  }
}

const Layer = emotion.div`
  position: fixed;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
`

const Backdrop = emotion.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  background-color: rgba(0, 0, 0, .5);
`

const Paper = emotion.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  width: 400px;
  height: 250px;
  background-color: white;
  box-shadow: 0 0 20px 0 rgba(0,0,0,0.30);
  border-radius: 3px;
`

const Heading = emotion.div`
  font-size: 24px;
  color: ${colors.dark};
  padding-top: 30px;
  text-align: center;
`

const Text = emotion.div`
  color: ${colors.dark};
  padding-top: 30px;
  text-align: center;
`

const Buttons = emotion.div`
  display: flex;
`

const Button = emotion.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 60px;
  cursor: pointer;
  border-radius: 3px;
  color: ${_ => _.color || colors.dark};
  font-size: 18px;
  font-weight: 500;
  &:hover {
    background-color: #EEE;
  }
`
