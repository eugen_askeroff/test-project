import emotion from '@emotion/styled/macro'
import color from 'color'

import { colors } from './theme'

export const Button = emotion.button`
  background-color: ${colors.green};
  border: none;
  outline: none;
  height: 40px;
  width: 150px;
  color: white;
  font-size: 14px;
  &:not(:disabled) {
    cursor: pointer;
    &:hover {
      background-color: ${color(colors.green).darken(0.15).hex()};
    }
    &:active {
      background-color: ${color(colors.green).darken(0.3).hex()};
    }
  }
  &:disabled {
    background-color: ${color(colors.green).lighten(0.7).desaturate(0.7).hex()};
  }
`
