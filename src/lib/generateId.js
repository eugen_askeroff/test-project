export function generateId(exceptions = []) {
  for (let i = 0; i < 20; i++) {
    const id = Math.random().toString(36).slice(2)
    if (!exceptions.includes(id)) {
      return id
    }
  }
  throw new Error('Can not generate unique id')
}
