import React from 'react'
import emotion from '@emotion/styled/macro'

import { requestMyIdeas } from '../../resources'
import { colors } from '../../ui/theme'
import { EmptyState } from './EmptyState'
import { Ideas } from './Ideas'
import { generateId } from '../../lib/generateId'
import { ListHead } from './ListHead'

export class IdeasPage extends React.PureComponent {
  static path = '/ideas'

  state = {
    ideas: [],
    ideasHasBeenLoaded: false,
  }

  async componentDidMount() {
    const { data } = await requestMyIdeas()
    this.setState({
      ideas: data,
      ideasHasBeenLoaded: true,
    })
  }

  Head = () =>
    <Head>
      <h2>My Ideas</h2>
      { this.state.ideasHasBeenLoaded &&
        <CreateButton onClick={this.createNewIdea}/>
      }
    </Head>

  changeIdeas = (mutation) =>
    this.setState({ ideas: mutation(this.state.ideas) })

  createNewIdea = () => {
    const newIdea = {
      id: generateId(this.state.ideas.map(_ => _.id)),
      content: '',
      impact: 1,
      ease: 1,
      confidence: 1,
      viewMode: 'CREATE',
      isNew: true,
    }
    this.setState({ ideas: [ newIdea, ...this.state.ideas ] })
  }

  render() {
    const s = this.state
    return (
      <Page>
        <this.Head/>
        { !!s.ideas.length && <ListHead/> }
        <Ideas ideas={s.ideas} changeIdeas={this.changeIdeas}/>
        { s.ideasHasBeenLoaded && s.ideas.length === 0 && <EmptyState/> }
      </Page>
    )
  }
}

const Page = emotion.div`
  padding: 0 80px;
`

const Head = emotion.div`
  height: 120px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid rgba(42,56,66,0.20);
  h2 {
    font-size: 28px;
    line-height: 28px;
    color: ${colors.dark};
    font-weight: 400;
    padding: 0;
    margin: 0;
  }
`

const CreateButton = emotion.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  cursor: pointer;
  background-image: image-set(
    url(https://small-project-api.herokuapp.com/mockup/web/assets/btn_addanidea.png) 1x,
    url(https://small-project-api.herokuapp.com/mockup/web/assets/btn_addanidea@2x.png) 2x
  );
  &:hover {
    opacity: 0.7;
  }
`
