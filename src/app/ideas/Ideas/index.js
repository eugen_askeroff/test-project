import React from 'react'
import emotion from '@emotion/styled/macro'

import { IdeaCreateView } from './IdeaCreateView'
import { IdeaUpdateView } from './IdeaUpdateView'
import { IdeaReadView } from './IdeaReadView'
import { Alert } from '../../../ui/Alert'

export class Ideas extends React.PureComponent {
  state = { modalIsShown: false }

  confirmIdeaDelete = (onConfirm) => {
    this.onConfirm = onConfirm
    this.setState({ modalIsShown: true })
  }

  closeModal = () => {
    this.onConfirm = undefined
    this.setState({ modalIsShown: false })
  }

  render() {
    return (
      <>
        <Self>
          { this.props.ideas.map(idea => {
            const Idea = (() => {
              switch (idea.viewMode) {
                case 'CREATE': return IdeaCreateView
                case 'UPDATE': return IdeaUpdateView
                case 'READ': return IdeaReadView
                default: return IdeaReadView
              }
            })()
            return <Idea
              key={idea.id}
              idea={idea}
              changeIdeas={this.props.changeIdeas}
              confirmIdeaDelete={this.confirmIdeaDelete}
            />
          })}
        </Self>
        { this.state.modalIsShown &&
          <Alert
            text="This idea will be permanently deleted."
            onCancel={this.closeModal}
            onConfirm={() => {
              this.onConfirm()
              this.closeModal()
            }}
          />
        }
      </>
    )
  }
}

const Self = emotion.ul`
  padding: 0;
  margin: 0;
`
