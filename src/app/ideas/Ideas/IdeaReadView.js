import React from 'react'
import { Idea } from './Idea'
import { requestToDeleteAnIdea } from '../../../resources'

export class IdeaReadView extends React.PureComponent {
  render() {
    const { idea } = this.props
    return (
      <Idea
        key={idea.id}
        {...idea}
        averageScore={(+idea.average_score).toFixed(1)}
        firstButton={{ icon: 'pen', onClick: this.onUpdateIntention, isShown: !idea.isNew }}
        secondButton={{ icon: 'bin', onClick: this.onDeleteIntention, isShown: !idea.isNew }}
      />
    )
  }

  onUpdateIntention = () =>
    this.props.changeIdeas(ideas => ideas.map(idea => {
      if (idea.id === this.props.idea.id) {
        return {...idea, viewMode: 'UPDATE'}
      }
      return idea
    }))

  onDeleteIntention = () => {
    requestToDeleteAnIdea(this.props.idea.id)
    this.props.confirmIdeaDelete(
      () => this.props.changeIdeas(
        ideas => ideas.filter(idea => idea.id !== this.props.idea.id)
      )
    )
  }
}
