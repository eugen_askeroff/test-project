import React from 'react'
import emotion from '@emotion/styled/macro'

export class NumberSelect extends React.PureComponent {
  render() {
    return (
      <Select
        value={this.props.value}
        onChange={(e) => this.props.onChange(e.target.value)}
      >
        { [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((n) =>
          <option
            selected={String(n) === this.props.value}
            key={n}
            value={n}
            children={n}
          />
        )}
      </Select>
    )
  }
}

const Select = emotion.select`
  height: 44px;
  width: 50px;
  background-image: linear-gradient(-180deg, #F2F2F2 0%, #D4D4D4 100%);
  border: 1px solid #979797;
  border-radius: 3px;
  padding-left: 7px;
`
