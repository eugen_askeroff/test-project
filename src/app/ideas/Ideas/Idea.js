import React from 'react'
import emotion from '@emotion/styled/macro'

export class Idea extends React.PureComponent {
  static defaultProps = { showButtons: true }

  render() {
    const p = this.props
    const showFirstButton = p.firstButton.isShown === undefined || p.firstButton.isShown
    const showSecondButton = p.secondButton.isShown === undefined || p.secondButton.isShown
    return (
      <Self>
        <Dot>•</Dot>
        <Content>{p.content}</Content>
        <Value>{p.impact}</Value>
        <Value>{p.ease}</Value>
        <Value>{p.confidence}</Value>
        <Value>{p.averageScore}</Value>
        <ButtonWrapper>
          { showFirstButton &&
            <Button
              onClick={p.firstButton.onClick}
              icon={p.firstButton.icon}
            />
          }
        </ButtonWrapper>
        <ButtonWrapper>
          { showSecondButton &&
            <Button
              onClick={p.secondButton.onClick}
              icon={p.secondButton.icon}
            />
          }
        </ButtonWrapper>
      </Self>
    )
  }
}

const Self = emotion.li`
  display: flex;
  align-items: center;
  height: 50px;
`

const Dot = emotion.div`
  width: 24px;
  color: rgba(42,56,66,0.4);
  font-size: 30px;
`

const Content = emotion.div`
  flex: 1;
  padding-right: 20px;
`

const Value = emotion.div`
  width: 80px;
`

const ButtonWrapper = emotion.div`
  display: flex;
  justify-content: center;
  width: 45px;
  cursor: pointer;
  &:hover {
    opacity: 0.7;
  }
`

const Button = emotion.div`
  width: 20px;
  height: 20px;
  background-image: image-set(
    url(https://small-project-api.herokuapp.com/mockup/web/assets/${_ => _.icon}.png) 1x,
    url(https://small-project-api.herokuapp.com/mockup/web/assets/${_ => _.icon}@2x.png) 2x
  );
  background-repeat: no-repeat;
  background-position: center;
`
