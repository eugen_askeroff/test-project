import React from 'react'

import { Idea } from './Idea'
import { NumberSelect } from './Select'
import { Input } from '../../../ui/Input'
import { requestToCreateANewIdea } from '../../../resources'

export class IdeaCreateView extends React.PureComponent {
  state = this.props.idea

  render() {
    return (
      <Idea
        key={this.state.id}
        content={<this.Input/>}
        impact={<this.NumberSelect prop="impact"/>}
        ease={<this.NumberSelect prop="ease"/>}
        confidence={<this.NumberSelect prop="confidence"/>}
        averageScore={this.averageScore}
        firstButton={{ icon: 'Confirm_V', onClick: this.confirmChanges, isShown: !!this.state.content }}
        secondButton={{ icon: 'Cancel_X', onClick: this.cancelChanges }}
      />
    )
  }

  get averageScore() {
    const s = this.state
    return (((+s.impact) + (+s.ease) + (+s.confidence)) / 3).toFixed(1)
  }

  confirmChanges = async () => {
    const newItem = {
      ...this.state,
      average_score: this.averageScore,
      viewMode: 'READ',
    }
    this.props.changeIdeas(ideas => ideas.map(idea => {
      if (idea.id === this.props.idea.id) return newItem
      return idea
    }))
    const { data: { id: newId } } = await requestToCreateANewIdea(newItem)
    this.props.changeIdeas(ideas => ideas.map(idea => {
      if (idea.id === this.props.idea.id) {
        return { ...idea, id: newId, isNew: false }
      }
      return idea
    }))
  }

  cancelChanges = () => this.props.changeIdeas(
    ideas => ideas.filter(idea => idea.id !== this.props.idea.id)
  )

  Input = () =>
    <Input
      value={this.state.content}
      onChange={(e) => this.setState({ content: e.target.value })}
    />

  NumberSelect = ({ prop }) =>
    <NumberSelect
      value={this.state[prop]}
      onChange={(val) => this.setState({ [prop]: val })}
    />
}
