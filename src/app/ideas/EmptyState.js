import React from 'react'
import emotion from '@emotion/styled/macro'

import { colors } from '../../ui/theme'

export class EmptyState extends React.PureComponent {
  render() {
    return (
      <Self>
        <Icon/>
        <Text>
          Got Ideas?
        </Text>
      </Self>
    )
  }
}

const Self = emotion.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 20vh;
`

const Icon = emotion.div`
  height: 100px;
  width: 70px;
  background-image: image-set(
    url(https://small-project-api.herokuapp.com/mockup/web/assets/bulb.png) 1x,
    url(https://small-project-api.herokuapp.com/mockup/web/assets/bulb@2x.png) 2x
  );
`

const Text = emotion.div`
  padding-top: 10px;
  font-size: 20px;
  color: ${colors.dark};
`
