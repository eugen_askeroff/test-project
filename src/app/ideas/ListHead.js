import React from 'react'
import emotion from '@emotion/styled/macro'

export class ListHead extends React.PureComponent {
  render() {
    return (
      <Self>
        <ColName>
          Impact
        </ColName>
        <ColName>
          Ease
        </ColName>
        <ColName>
          Confidence
        </ColName>
        <ColName>
          <strong>Avg.</strong>
        </ColName>
      </Self>
    )
  }
}

const Self = emotion.div`
  display: flex;
  justify-content: flex-end;
  padding-right: 90px;
  padding-top: 40px;
  padding-bottom: 10px;
`

const ColName = emotion.div`
  width: 80px;
  font-size: 14px;
`
