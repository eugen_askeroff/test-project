import React from 'react'
import emotion from '@emotion/styled/macro'
import { connect } from 'react-redux'
import md5 from 'md5'

import { requestMyData } from '../../resources'
import { logOut } from '../../system/auth'

export class UserComponent extends React.PureComponent {
  state = { me: undefined }

  async componentDidMount() {
    const { data } = await requestMyData()
    this.setState({ me: data })
  }

  render() {
    const { me } = this.state
    if (!me) return null
    return (
      <Self>
        <this.Avatar/>
        <Name>
          {me.name}
        </Name>
        <LogOutButton onClick={this.props.logOut}>
          Log Out
        </LogOutButton>
      </Self>
    )
  }

  Avatar = () => {
    const { me } = this.state
    return <Avatar
      src={getGravatar(me.email, 64)}
      srcSet={`${getGravatar(me.email, 128)} 2x`}
      alt={me.name}
    />
  }
}

export const User = connect(state => state, { logOut })(UserComponent)

const Self = emotion.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  border-top: 1px solid rgba(255,255,255,0.20);
  padding-top: 37px;
`

const Avatar = emotion.img`
  border-radius: 50%;
`

const Name = emotion.div`
  color: white;
  font-size: 20px;
`

const LogOutButton = emotion.div`
  margin-top: 9px;
  font-weight: 500;
  font-size: 16px;
  color: rgba(42,56,66,0.65);
  cursor: pointer;
`


function getGravatar(email, size) {
  const hash = md5(email.trim().toLowerCase())
  return `https://www.gravatar.com/avatar/${hash}?size=${size}`
}
