import React, { Component } from 'react'
import emotion from '@emotion/styled/macro'

import { User } from './User'
import connect from 'react-redux/es/connect/connect'

export class LayoutComponent extends Component {

  LogoBlock = () =>
    <Wrapper>
      <div>
        <Logo/>
      </div>
      <Text>
        The Idea Pool
      </Text>
    </Wrapper>

  render() {
    return (
      <Self>
        <LeftColumn>
          <this.LogoBlock/>
          { this.props.isAuth && <User/> }
        </LeftColumn>
        <Content>
          { this.props.children }
        </Content>
      </Self>
    );
  }
}

export const Layout = connect(state => state)(LayoutComponent)

const Self = emotion.div`
  display: flex;
  min-height: 100vh;
`

const LeftColumn = emotion.div`
  width: 200px;
  background-color: rgb(0,168,67);
  padding: 0 27px;
`

const Wrapper = emotion.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 37px 27px 44px;
`

const Text = emotion.span`
  color: white;
  margin-top: 7px;
  font-size: 16px;
`

const Content = emotion.div`
  flex: 1;
`

function Logo() {
  const stdLogo = 'https://small-project-api.herokuapp.com/mockup/web/assets/IdeaPool_icon.png'
  const doubleSizeLogo = 'https://small-project-api.herokuapp.com/mockup/web/assets/IdeaPool_icon@2x.png'
  return (
    <img
      src={stdLogo}
      srcSet={`${doubleSizeLogo} 2x`}
      alt="Logo"
    />
  )
}
