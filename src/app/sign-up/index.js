import { connect } from 'react-redux'
import { SignUpPage as Page } from './SignUpPage'
import { signUp } from '../../system/auth'

export const SignUpPage = connect(
  undefined,
  { signUp }
)(Page)
