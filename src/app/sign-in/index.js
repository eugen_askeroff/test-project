import { connect } from 'react-redux'
import { SignInPage as Page } from './SignInPage'
import { logIn } from '../../system/auth'

export const SignInPage = connect(
  undefined,
  { logIn }
)(Page)
