import React from 'react'
import emotion from '@emotion/styled/macro'
import { Formik, Form as FormikForm, Field } from 'formik'

import { SignUpPage } from '../sign-up'
import { Input } from '../../ui/Input'
import { Button } from '../../ui/Button'
import { Link } from '../../ui/Link'
import { colors } from '../../ui/theme'

export class SignInPage extends React.PureComponent {
  static path = '/sign-in'

  state = { error: '' }

  render() {
    return (
      <PageLayout>
        <Content>
          <Heading>
            Log In
          </Heading>
          <Error>
            {this.state.error}
          </Error>
          <Formik
            initialValues={{ email: '', password: '' }}
            onSubmit={this.onSubmit}
          >
            {({ isSubmitting }) =>
              <Form>
                <this.Field name="email" placeholder="Email" type="email"/>
                <this.Field name="password" placeholder="Password" type="password"/>
                <BottomBlock>
                  <Button disabled={isSubmitting}>
                    { isSubmitting ? 'LOADING' : 'LOG IN' }
                  </Button>
                  <span>
                    Don't have an account?{' '}
                    <Link to={SignUpPage.path}>Create an account</Link>
                  </span>
                </BottomBlock>
              </Form>
            }
          </Formik>
        </Content>
      </PageLayout>
    )
  }

  Field = (props) =>
    <Field
      name={props.name}
      render={({ field }) =>
        <FormInput {...props} {...field} />
      }
    />

  onSubmit = async ({ email, password }, actions) => {
    this.setState({ error: '' })
    actions.setSubmitting(true)
    try {
      await this.props.logIn(email, password)
    }
    catch (e) {
      const { reason } = e.response.data
      this.setState({ error: reason })
    }
    finally {
      actions.setSubmitting(false)
    }
  }
}

const PageLayout = emotion.div`
  display: flex;
  justify-content: center;
  padding-top: 217px;
`

const Content = emotion.div`
  width: 480px;
`

const Heading = emotion.h2`
  font-weight: normal;
  font-size: 40px;
  color: ${colors.dark};
  text-align: center;
  padding: 0;
  margin: 0;
`

const Error = emotion.div`
  color: red;
  min-height: 54px;
  font-size: 14px;
  text-align: center;
`

const Form = emotion(FormikForm)`
  display: flex;
  flex-direction: column;
`

const FormInput = emotion(Input)`
  margin-bottom: 44px;
`

const BottomBlock = emotion.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 16px;
`
