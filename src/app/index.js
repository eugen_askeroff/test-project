import React, { Component } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom'
import { connect, Provider } from 'react-redux'

import { SignInPage } from './sign-in'
import { SignUpPage } from './sign-up'
import { IdeasPage } from './ideas'
import { store } from '../system/store'
import { Layout } from './layout'

export class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Layout>
          <BrowserRouter>
            <>
              <Redirects/>
              <Switch>
                <Route exact path={SignInPage.path} component={SignInPage} />
                <Route exact path={SignUpPage.path} component={SignUpPage} />
                <Route exact path={IdeasPage.path} component={IdeasPage} />
              </Switch>
            </>
          </BrowserRouter>
        </Layout>
      </Provider>
    );
  }
}

const Redirects = connect(state => state)(
  class Redirects extends React.PureComponent {
    render() {
      if (this.props.isAuth) {
        return <>
          <Redirect exact from="/" to={IdeasPage.path}/>
          <Redirect exact path={SignInPage.path} to={IdeasPage.path}/>
          <Redirect exact path={SignUpPage.path} to={IdeasPage.path}/>
        </>
      }
      return <>
        <Redirect exact from="/" to={SignInPage.path}/>
        <Redirect exact path={IdeasPage.path} to={SignInPage.path}/>
      </>
    }
  }
)
