import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import thunk from 'redux-thunk'

import { authReducer } from './auth'

const rootReducer = combineReducers({
  isAuth: authReducer
})

const devToolsMiddleware = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : _ => _

export const store = createStore(
  rootReducer,
  compose(applyMiddleware(thunk), devToolsMiddleware),
)
