import axios from 'axios'

import { store } from './store'
import { logOut } from './auth'
import { requestTokenRefresh } from '../resources'

export const baseURL = 'https://small-project-api.herokuapp.com'

export function configurateAxios() {
  axios.defaults.baseURL = baseURL

  axios.interceptors.request.use(
    (config) => {
      if (localStorage.accessToken) {
        config.headers['x-access-token'] = localStorage.accessToken
      }
      return config
    },
    (error) => Promise.reject(error),
  )

  axios.interceptors.response.use(
    (_) => _,
    async (error) => {
      if (error.response.status === 401) {
        try {
          const res = await requestTokenRefresh(localStorage.refreshToken)
          localStorage.accessToken = res.data.jwt
          return axios.request(error.config)
        } catch (e) {
          store.dispatch(logOut())
          return Promise.reject(error)
        }
      }
      return Promise.reject(error)
    },
  )
}
