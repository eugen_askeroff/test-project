import { createAction, createReducer } from 'redux-act'
import { requestToLogIn, requestToLogOut, requestToSignUp } from '../resources'

export const signUp = (name, email, password) => async (dispatch) => {
  const { data } = await requestToSignUp(name, email, password)
  localStorage.accessToken = data.jwt
  localStorage.refreshToken = data.refresh_token
  dispatch(setLoggedIn())
}

export const logIn = (email, password) => async (dispatch) => {
  const { data } = await requestToLogIn(email, password)
  localStorage.accessToken = data.jwt
  localStorage.refreshToken = data.refresh_token
  dispatch(setLoggedIn())
}

export const logOut = () => async (dispatch) => {
  requestToLogOut(localStorage.accessToken, localStorage.refreshToken)
  delete localStorage.accessToken
  delete localStorage.refreshToken
  dispatch(setLoggedOut())
}

const setLoggedIn = createAction('System:setLoggedIn')
const setLoggedOut = createAction('System:setLoggedOut')

const initialState = !!localStorage.accessToken

export const authReducer = createReducer({
  [setLoggedIn]: () => true,
  [setLoggedOut]: () => false,
}, initialState)

