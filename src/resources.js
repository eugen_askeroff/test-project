import axios from 'axios'

import { baseURL } from './system/configurateAxios'

export const requestTokenRefresh = (refresh_token) =>
  axios.post('/access-tokens/refresh', { refresh_token })

export const requestToLogIn = (email, password) =>
  axios.post('/access-tokens', { email, password })

export const requestToLogOut = (accessToken, refresh_token) =>
  fetch(`${baseURL}/access-tokens`, {
    headers: {
      'Content-Type': 'application/json',
      'X-Access-Token': accessToken,
    },
    method: 'DELETE',
    body: JSON.stringify({ refresh_token })
  })

export const requestToSignUp = (name, email, password) =>
  axios.post('/users', { name, email, password })

export const requestMyData = () =>
  axios.get('/me')

export const requestToCreateANewIdea = ({ content, impact, ease, confidence }) =>
  axios.post('/ideas', { content, impact, ease, confidence })

export const requestToDeleteAnIdea = (ideaId) =>
  axios.delete(`/ideas/${ideaId}`)

export const requestMyIdeas = () =>
  axios.get(`/ideas?size=1000`)

export const requestToChangeAnIdea = (ideaId, { content, impact, ease, confidence }) =>
  axios.put(`/ideas/${ideaId}`, { content, impact, ease, confidence })
